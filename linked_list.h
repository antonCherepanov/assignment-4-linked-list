#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

struct node;

struct linked_list;

struct node *create_node(int value);

struct linked_list* create_list();

struct linked_list* create_list_from_array(int* array, size_t size);

void list_free(struct linked_list* list);

int list_length(struct linked_list* list);

struct node* list_node_at(struct linked_list* list, int index);

long list_sum(struct linked_list* list);

int list_get(struct linked_list* list, int index);

void list_add_front(struct linked_list* list, int value);

void list_add_back(struct linked_list* list, int value);

#endif