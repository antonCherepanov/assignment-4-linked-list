#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include "linked_list.h"

struct node {
    int value;
    struct node* previous;
    struct node* next;
};


struct linked_list {
    struct node* first;
    struct node* last;
    int size;
};

struct node* create_node(int value) {
    struct node* res = malloc(sizeof(struct node));
    res->value = value;
    res->next = NULL;
    res->previous = NULL;
    return res;
}

struct linked_list* create_list(){
	struct linked_list* list = malloc(sizeof(struct linked_list));
	list->first = NULL;
	list->last = NULL;
	list->size = 0;
	return list;
}

struct linked_list* create_list_from_array(int* array, size_t size){
	struct linked_list* list = malloc(sizeof(struct linked_list));
	for (size_t i = 0; i < size; i++) {
        list_add_back(list, array[i]);
    }
    return list; 
} 

void list_free(struct linked_list* list){
	struct node* current_node = list->first;
   	struct node* next_node;
    while (current_node) {
        next_node = current_node->next;
    	free(current_node);
    	current_node = next_node;
    };
	free(list);
}

int list_length(struct linked_list* list){
	return list->size;
}

struct node* list_node_at(struct linked_list* list, int index){
	int size = list->size; 
	struct node* element;
	if(size <= index || index < 0) return NULL;
	if(index <= size/2){
		element = list->first;
		for(int i = 0; i < index; i++){
			element=element->next;
		}
	}else{
		element = list->last;
		for(int i = size-1; i > index; i--){
			element=element->previous;
		}
	}
	return element;
}

long list_sum(struct linked_list* list){
	int size = list->size; 
	struct node* element = list->first;
	long sum = 0L;
	for(int i = 0; i < size; i++){
		sum+=element->value;
		element=element->next;
	}
	return sum;
}


int list_get(struct linked_list* list, int index){
	struct node* element = list_node_at(list, index);
	if(element == NULL) return 0;
	return element->value;
}

void list_add_front(struct linked_list* list, int value){
	struct node* element = create_node(value);
	struct node* first = list->first;
	element->next = first;
	if(list->size == 0){
		list->last = element;
	}else{
		first->previous = element;
	}
	list->first = element;
	list->size++;
}

void list_add_back(struct linked_list* list, int value){
	struct node* element = create_node(value);
	struct node* last = list->last;
	element->previous = last;
	if(list->size == 0){
		list->first = element;
	}else{
		last->next = element;
	}
	list->last = element;
	list->size++;
}