gcc -c -pedantic-errors -Wall -Werror -o $1.o $1.c -lm
gcc -c -pedantic-errors -Wall -Werror -o $2.o $2.c -lm
gcc -o $1 $1.o $2.o