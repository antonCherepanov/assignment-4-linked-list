#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "linked_list.h"

#define BUF_SIZE 300

static size_t elements_number; 

bool check_exit(char* input){
	return !(strcmp(input, "quit\n") && strcmp(input, "q\n") && strcmp(input, "quit") && strcmp(input, "q"));
}

int* read_ints(char *string) {
    int* array = malloc(sizeof(int) * 100);
    size_t i = 0;
    size_t j = 0;
    size_t length = strlen(string);
    int current = 0;
    bool is_negative = false;
    elements_number = 0;
    while (j < length) {
        if (string[j] == '-') {
            is_negative = true;
        } else if (string[j] >= '0' && string[j] <= '9') {
            current *= 10;
            current += string[j] - 48;
        } else {
            if (is_negative) {
                current = -current;
                is_negative = false;
            }
            array[i++] = current;
            elements_number++;
            current = 0;
        }
        j++;
    }
    return array;
}



int main(){
	int index;
	char input[BUF_SIZE];
	puts("Enter integer numbers separated by «SPACE» character");
	fgets(input, BUF_SIZE, stdin);
	if(check_exit(input)) return 0;
	int* elements = read_ints(input);
	struct linked_list* list = create_list_from_array(elements, elements_number);
	printf("The sum of the elements is equal to %ld\n", list_sum(list));
	while(true){
		puts("Enter the index of the element to return its value");
		scanf("%s", input);
		if(check_exit(input)) return 0;
		index = atoi(input);
		if(index || strcmp(input,"0") == 0){
			printf("The value of the element with the index %d is %d\n", index, list_get(list, index));
		}else{
			puts("You've provided an improper argument");
		}
	}
}